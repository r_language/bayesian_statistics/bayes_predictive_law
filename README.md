# Bayes_Predictive_Law

In this rmd file you can have a look at an exercise using Bayes method. We suppose that the presence of a disease for a patient follows a Bernoulli law with the parameters $\theta$ ($X \sim \mathcal{B}(\theta)$, $X \in \{0;1\}$), and we have a Beta a priori on $\theta$ : $\theta \sim \mathcal{B}e\left(\frac{1}{2};\frac{1}{2}\right)$

1. Simulate realizations of $\tilde x$ with two steps : 
    a. Draw of $\theta$
    b. Draw of $\tilde{x}|\theta$

2. Based on those simulations calculate the mean and variance of $\tilde{x}$

3. The calculation of this predictive law can be done like this :

4. What is the a posteriori distribution of $\theta$ ?

5. Give a credibility interval at 95% for $\theta$.

6. For a new patient with unknown status $\tilde{x}$, give the predictive distribution of $\tilde x$ : 

7. Extension to the binomial case


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

December 2022
